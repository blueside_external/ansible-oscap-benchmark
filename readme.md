# Ansible Role - Oscap CIS Benchmark

The following Ansible role, deploys OpenSCAP tooling and templates for
benchmarking a unix host against available OpenScap profiles.

## Context

The following role was created for the purpose of hardening Unix systems
(Specifically Rocky/RHEL 8.7) to the CIS L1 benchmark. This role assumes
you have built and are deploying a unix image locally on your system using
vagrant.

## Utilising the Role

* The role was
* Tasks were created to run benchmarks for both Rocky 8 and 9 based distros.
* You will need to tweak the tasks if you are using Alma, Redhat etc.
* This role can be run without any dependency, it outputs the standard HTML
  OpenScap report to the target hosts "/vagrant" directory so you can quickly
  evaluate security posture of a vagrant deployed host image.
